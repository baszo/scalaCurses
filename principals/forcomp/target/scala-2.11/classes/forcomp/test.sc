
object test{
  val aa = "aaabas"

  def wordOccurrences(w: String): Map[Char,Int] = {
    {
      for {
        (char, str: String) <- w.groupBy(lt => lt)
      } yield char -> str.length
    }.toList.sorted.toMap
  }

  wordOccurrences(aa)

  Seq("ate", "eat", "tea").groupBy(wordOccurrences)
    //w.groupBy{lt => lt}.map{(a:Char,b:String)=> a->b.length}

  def combinations(occurrences: List[(Char, Int)]): List[List[(Char, Int)]] = {
    for {
      (ch,occ) <- occurrences
      i <- 0 to occ
      rest = occurrences.filter(y => y._1 != ch)
      param =  if(rest.isEmpty) List(List()) else combinations(rest)
      r <- param
    } yield if(i==0) r else (ch,i) :: r
  }

  combinations(List(('a', 2), ('b', 2)))
}