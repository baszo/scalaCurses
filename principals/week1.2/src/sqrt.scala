/**
  * Created by andrzej2 on 11.10.16.
  */
/**
  * Created by andrzej2 on 11.10.16.
  */
object sqrt {

  def main(args: Array[String]) = sqrt(2)

  def abs(x: Double) = if (x < 0) -x else x

  def sqrtIter(guess: Double, x: Double): Double =
    if (isGoodEnough(guess, x)) guess
    else sqrtIter(improve(guess, x), x)

  def isGoodEnough(guess: Double, x: Double) = true

  def improve(guess: Double, x: Double) = (guess + x / guess) / 2

  def sqrt(x: Double) = sqrtIter(1.0, x)

  sqrt(4)


}



