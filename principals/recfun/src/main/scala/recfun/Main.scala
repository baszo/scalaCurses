package recfun

object Main {
  def main(args: Array[String]) {
//    println("Pascal's Triangle")
//    for (row <- 0 to 10) {
//      for (col <- 0 to row)
//        print(pascal(col, row) + " ")
//      println()
//    }
    print(countChange(300,List(5,10,20,50,100,200,500)))
  }

  /**
   * Exercise 1
   */
    def pascal(c: Int, r: Int): Int =
    if(c == 0 || r == 0 || r == c) 1 else pascal(c-1,r-1) + pascal(c,r-1)

  /**
   * Exercise 2
   */
    def balance(chars: List[Char]): Boolean = {

    def changeOccurrence(char: Char, list: List[Char], open: Int ): Boolean = {
        val openNew = char match {
          case '(' => open+1
          case ')' => open-1
          case _ => open
        }
      if(list.isEmpty || openNew < 0) openNew == 0
      else
        changeOccurrence(list.head,list.tail,openNew)
    }
    changeOccurrence(chars.head,chars.tail,0)

  }

  /**
   * Exercise 3
   */
    def countChange(money: Int, coins: List[Int]): Int = {

      def count(amount: Int, restCs: List[Int], possibilities: Int): Int =
        if(amount <= 0) possibilities
        else if(restCs.isEmpty) {
          if(amount == 0) possibilities + 1 else possibilities
        }
        else
          count(amount, restCs.tail, possibilities) + count(amount-restCs.head, restCs, possibilities)

      count(money, coins.filter(_ < money).sortWith(_ > _), 0)
    }

  }
