package quickcheck

import java.util.NoSuchElementException

import org.scalacheck.Arbitrary._
import org.scalacheck.Gen._
import org.scalacheck.Prop._
import org.scalacheck._

abstract class QuickCheckHeap extends Properties("Heap") with IntHeap {

  lazy val genHeap: Gen[H] = for {
    a <- arbitrary[Int]
    h <- oneOf(const(empty), genHeap)
  } yield insert(a, h)

  def toList(h: H): List[Int] = if (isEmpty(h)) Nil else findMin(h) :: toList(deleteMin(h))


  implicit lazy val arbHeap: Arbitrary[H] = Arbitrary(genHeap)

  property("gen1") = forAll { (h: H) =>
    val m = if (isEmpty(h)) 0 else findMin(h)
    findMin(insert(m, h)) == m
  }

  property("min1") = forAll { a: Int =>
    val h = insert(a, empty)
    findMin(h) == a
  }

  property("order") = forAll { (a: Int, b: Int) =>
    val h = insert(a, insert(b, empty))
    findMin(h) == (if (a > b) b else a)
  }

  def createMelded(a: Int, b: Int) = {
    val h = insert(a, empty)
    val i = insert(b, empty)
    meld(h, i)
  }

  property("melding") = forAll { (a: Int, b: Int) =>
    val merged = createMelded(a, b)
    findMin(merged) == (if (a > b) b else a)
  }

  property("meldWithEmpty") = forAll { h: H =>
    meld(h, empty) == h
  }

  property("removeEmpty") = forAll { a: Int =>
    try {
      deleteMin(empty) == empty
    }
    catch {
      case e:NoSuchElementException => true
      case _ => false
    }

  }

  property("melding and insert") = forAll { (a: Int, b: Int, c: Int) =>
    val merged = createMelded(a, b)
    val mergedAndInsert = insert(c, merged)
    val minOfHeap = List(a, b, c).min
    findMin(mergedAndInsert) == minOfHeap
  }

  property("delate one") = forAll { a: Int =>
    val h = insert(a, empty)
    val h1 = deleteMin(h)
    h1 == empty
  }

  property("insert Two elements") = forAll { (a: Int, b: Int) =>
    val h = insert(b, insert(a, empty))
    val smallest = Math.min(a, b)
    findMin(h) == smallest
  }

  property("meldMin") = forAll { (h1: H, h2: H) =>
    val min1 = findMin(h1)
    val min2 = findMin(h2)
    val m = meld(h1, h2)
    val minMeld = findMin(m)
    minMeld == min1 || minMeld == min2
  }

  property("associative meld") = forAll { (h: H, i: H, j: H) =>
    val a = meld(meld(h, i), j)
    val b = meld(h, meld(i, j))
    toList(a) == toList(b)
  }

  property("remMin") = forAll { h: H =>
    def remMin(ts: H, as: List[Int]): List[Int] = {
      if (isEmpty(ts)) as
      else findMin(ts) :: remMin(deleteMin(ts), as)
    }
    val xs = remMin(h, Nil)
    xs == xs.sorted
  }

}
