package reductions

import scala.annotation._
import org.scalameter._
import common._

object ParallelParenthesesBalancingRunner {

  @volatile var seqResult = false

  @volatile var parResult = false

  val standardConfig = config(
    Key.exec.minWarmupRuns -> 40,
    Key.exec.maxWarmupRuns -> 80,
    Key.exec.benchRuns -> 120,
    Key.verbose -> true
  ) withWarmer(new Warmer.Default)

  def main(args: Array[String]): Unit = {
    val length = 100000000
    val chars = new Array[Char](length)
    val threshold = 10000
    val seqtime = standardConfig measure {
      seqResult = ParallelParenthesesBalancing.balance(chars)
    }
    println(s"sequential result = $seqResult")
    println(s"sequential balancing time: $seqtime ms")

    val fjtime = standardConfig measure {
      parResult = ParallelParenthesesBalancing.parBalance(chars, threshold)
    }
    println(s"parallel result = $parResult")
    println(s"parallel balancing time: $fjtime ms")
    println(s"speedup: ${seqtime / fjtime}")
  }
}

object ParallelParenthesesBalancing {

  /** Returns `true` iff the parentheses in the input `chars` are balanced.
   */
  def balance(chars: Array[Char]): Boolean = {
      def checkOpenClose(chars: Array[Char], openCount: Int): Boolean = {
        if (chars.isEmpty) openCount == 0
        else if (openCount < 0) false
        else if (chars.head == '(') checkOpenClose(chars.tail, openCount + 1)
        else if (chars.head == ')') checkOpenClose(chars.tail, openCount - 1)
        else checkOpenClose(chars.tail, openCount)

      }
      checkOpenClose(chars, 0)
  }

  /** Returns `true` iff the parentheses in the input `chars` are balanced.
   */
  def parBalance(chars: Array[Char], threshold: Int): Boolean = {

    def traverse(idx: Int, until: Int, bal: Int, sign: Int) : (Int,Int) = {
      if (idx < until) {
        chars(idx) match {
          case '(' => traverse(idx + 1, until, bal + 1, sign)
          case ')' =>
            if (bal > 0) traverse(idx + 1, until, bal - 1, sign)
            else traverse(idx + 1, until, bal, sign + 1)
          case _ => traverse(idx + 1, until, bal, sign)
        }
      } else (bal, sign)
    }

    def reduce(from: Int, until: Int): (Int,Int) = {
      if(until - from <= threshold)
        traverse(from,until,0,0)
      else {
        val middle = from + ((until - from) / 2)
        val ((b1,s1),(b2,s2)) = parallel(reduce(from,middle),reduce(middle,until))
        (b1+b2,s1+s2)
      }
    }

    reduce(0, chars.length) == (0,0)
  }

  // For those who want more:
  // Prove that your reduction operator is associative!

}
